import java.util.ArrayList;
import java.util.Arrays;

public class Desafio03 {

    public static void main(String[] args) {
        int[] numeros = {4, -5, 8, -10};
        ArrayList<Integer> numerosaux = new ArrayList();
        Arrays.sort(numeros);
        int negativos = 0;
        // Checando os negativos e adicionando todos os positivos no novo array
        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i] < 0) {
                negativos++;
            }
            if (numeros[i] > 0) {
                numerosaux.add(numeros[i]);
            }
        }
        // Adicionando os negativos no novo array
        if (negativos > 0) {
            if (negativos % 2 != 0) {
                for (int i = 0; i < negativos - 1; i++) {
                    numerosaux.add(numeros[i]);
                }
            } else {
                for (int i = 0; i < negativos; i++) {
                    numerosaux.add(numeros[i]);
                }
            }
        }
         // Multiplicando
        int valor = 0;
        for (Integer i : numerosaux) {
            if (valor == 0) {
                valor = i;
            } else {
                valor *= i;
            }
        }
        System.out.println("Produto: " + valor);
        System.out.println("Valores: " + numerosaux);
    }
}
