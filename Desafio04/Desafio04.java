

public class Desafio04 {

    public static int contadorPeca(int[] tabuleiro, int peca) {
        int valor = 0;

        for (int i = 0; i < tabuleiro.length; i++) {
            while (tabuleiro[i] == peca) {
                valor++;
                break;
            }
        }
        return valor;
    }

    public static void main(String[] args) {
        int[] tabuleiro = {4, 3, 2, 5, 6, 2, 3, 4,
                1, 1, 1, 1, 1, 1, 1, 1,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                1, 1, 1, 1, 1, 1, 1, 1,
                4, 3, 2, 5, 6, 2, 3, 4};

        System.out.println("Existem " + contadorPeca(tabuleiro, 6) + " rainhas no jogo.");
        System.out.println("Existem " + contadorPeca(tabuleiro, 5) + " reis no jogo.");
        System.out.println("Existem " + contadorPeca(tabuleiro, 4) + " torres no jogo.");
        System.out.println("Existem " + contadorPeca(tabuleiro, 3) + " cavalos no jogo.");
        System.out.println("Existem " + contadorPeca(tabuleiro, 2) + " bispos no jogo.");
        System.out.println("Existem " + contadorPeca(tabuleiro, 1) + " peões no jogo.");
        System.out.println("Existem " + contadorPeca(tabuleiro, 0) + " espaços vazios no jogo.");

    }
}