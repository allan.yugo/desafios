import java.util.ArrayList;

public class Desafio05 {


    public static void main(String[] args) {
        ArrayList<Float> cargos = new ArrayList();
        Programa programa = new Programa();
        programa.cadastrarCargo(cargos, 2500);
        programa.cadastrarCargo(cargos, 1500);
        programa.cadastrarCargo(cargos, 10000);
        programa.cadastrarCargo(cargos, 1200);
        programa.cadastrarCargo(cargos, 800);
        programa.cadastrarCargo(cargos, 900);
        programa.cadastrarFuncionario(cargos, 15, "João da Silva", 1);
        programa.cadastrarFuncionario(cargos, 1, "Pedro Santos", 2);
        programa.cadastrarFuncionario(cargos, 26, "Maria Oliveira", 3);
        programa.cadastrarFuncionario(cargos, 12, "Rita Alcântara", 5);
        programa.cadastrarFuncionario(cargos, 8, "Lígia Matos", 2);
        programa.mostrarRelatorio(cargos);
        programa.mostrarSalarioCargo(cargos, 2);

        // Cadastrar Funcionário porém será colocado um código de Funcionário Repetido.
        programa.cadastrarFuncionario(cargos, 26, "Jose Carlos", 2);
        // Cadastrar Funcionário porém o código do cargo não existe.
        programa.cadastrarFuncionario(cargos, 30, "Julia Soares", 10);
        // Mostrar total de salário por cargo porém o cargo nao existe.
        programa.mostrarSalarioCargo(cargos, 9);


    }

}
