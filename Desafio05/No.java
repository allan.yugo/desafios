public class No {
    public int posicao;
    public No proximo;
    private int codigoFuncionario;
    private String nomeFuncionario;
    private int codigoCargoFuncionario;


    public No(int posicao) {
        this.posicao = posicao;
        proximo = null;
    }

    public int getCodigoFuncionario() {
        return codigoFuncionario;
    }

    public void setCodigoFuncionario(int codigoFuncionario) {
        this.codigoFuncionario = codigoFuncionario;
    }

    public String getNomeFuncionario() {
        return nomeFuncionario;
    }

    public void setNomeFuncionario(String nomeFuncionario) {
        this.nomeFuncionario = nomeFuncionario;
    }

    public int getCodigoCargoFuncionario() {
        return codigoCargoFuncionario;
    }

    public void setCodigoCargoFuncionario(int codigoCargoFuncionario) {
        this.codigoCargoFuncionario = codigoCargoFuncionario;
    }
}