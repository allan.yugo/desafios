import java.util.ArrayList;

public class Programa {
    private No primeiro;
    private No ultimo;
    int posicao = 0;

    public Programa() {
        primeiro = null;
        ultimo = null;
    }

    public void cadastrarCargo(ArrayList<Float> cargos, float salario) {
        cargos.add(salario);
    }

    public void cadastrarFuncionario(ArrayList<Float> cargos, int codigoFuncionario, String nomeFuncionario, int codigoCargoFuncionario) {
        No novo = new No(posicao);
        novo.setCodigoFuncionario(codigoFuncionario);
        novo.setNomeFuncionario(nomeFuncionario);
        novo.setCodigoCargoFuncionario(codigoCargoFuncionario);
        posicao++;

        if (primeiro == null) {
            primeiro = novo;
            ultimo = novo;
        } else if (verificarCodigoFuncionario(codigoFuncionario) == true) {
            if (codigoCargoFuncionario > cargos.size() - 1) {
                System.out.println("Impossivel cadastrar! Código de cargo não existe.");
            }  else {
                ultimo.proximo = novo;
                ultimo = novo;
            }
        }
    }

    public boolean verificarCodigoFuncionario(int codigoFuncionario) {
        No atual = primeiro;
        boolean condicao = true;
        while (atual != null) {
            if (atual.getCodigoFuncionario() == codigoFuncionario) {
                System.out.println("Impossivel cadastrar! Código Funcionário já existe.");
                condicao = false;
            }
            atual = atual.proximo;
        }
        return condicao;
    }

    public void mostrarRelatorio(ArrayList<Float> cargos) {
        No atual = primeiro;
        System.out.printf("%-20s%-20s%s\n", "Código Funcionário", "Nome Funcionário", "Salário");
        while (atual != null) {
            for (int i = 0; i < cargos.size(); i++) {
                if (atual.getCodigoCargoFuncionario() == i) {
                    System.out.printf("%-20s%-20s%s\n", atual.getCodigoFuncionario(), atual.getNomeFuncionario(), cargos.get(i));
                }
            }
            atual = atual.proximo;
        }
        System.out.println();
    }

    public void mostrarSalarioCargo(ArrayList<Float> cargos, int codigoCargoFuncionario) {
        float contador = 0;
        No atual = primeiro;
        if (codigoCargoFuncionario > cargos.size() - 1) {
            System.out.println("Impossivel Consultar! Código do cargo não existe.");
        } else {
            while (atual != null) {
                if (atual.getCodigoCargoFuncionario() == codigoCargoFuncionario) {
                    contador = contador + cargos.get(codigoCargoFuncionario);
                }
                atual = atual.proximo;

            }
            System.out.println("Valor total pago para o grupo " + codigoCargoFuncionario + " de funcionários | R$" + contador);
        }
    }
}
