public class Desafio06 {


    public static void calcularMaior(int[] sequencia) {
        int posicaoInicial = 0;
        int posicaoFinal = 0;
        int posicaoInicialAux = 0;
        int posicaoFinalAux = 0;
        int maiorSequencia = 0;
        int maiorSequenciaAux = 0;
        int posicao = 0;

        for (int i = 0; i < sequencia.length; i++) {
            if (sequencia[i] == 1) {
                maiorSequencia++;
                if (posicaoInicial == 0) { //Caso a a posical "i" corresponda a um "1" na sequencia, ele automaticamente seta a posicaoInicial = i+1(porque não estamos tratando da posicao no array e sim nos numeros).
                    posicaoInicial = i + 1;
                    posicaoFinal = posicaoInicial;
                } else {
                    posicaoFinal++;
                }
            } else if (sequencia[i] == 0) {
                if (maiorSequencia > maiorSequenciaAux) { //Confere caso a maiorSequencia que está acontecendo é maior que a maiorSequenciaAux que corresponde a maiorSequencia de 1's anterior antes de existir algum 0.
                    maiorSequenciaAux = maiorSequencia;
                    posicaoInicialAux = posicaoInicial;
                    posicaoFinalAux = posicaoFinal;
                    if (sequencia[posicaoFinalAux + 1] == 1) { // //Conferir se o anterior da posicaoFinalAux é 0 e o anterior dele é 1, para que se incremente e vire a maior sequencia.
                        posicao = posicaoFinalAux + 1;
                        posicaoFinalAux = posicao + 1;
                        maiorSequenciaAux = maiorSequenciaAux + 2;
                    } else { // Caso a conferencia anterior nao ocorra, optei por colocar o  "1" na esquerda(posicalAux-1).
                        posicaoInicialAux--;
                        maiorSequenciaAux++;
                        posicao = posicaoInicialAux;
                    }
                    maiorSequencia = 0;
                    posicaoInicial = 0;
                    maiorSequencia = 0;
                }
                maiorSequencia = 0;
                posicaoInicial = 0;
                posicaoFinal = 0;
            }
        }
        System.out.println(posicao);
    }
    public static void main(String[] args) {
        int[] sequencia = {0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1};

        calcularMaior(sequencia);
    }
}
